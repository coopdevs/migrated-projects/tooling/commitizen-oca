## 0.3.1 (2022-03-22)


- [REL]: version 0.3.0 → 0.3.1
- [IMP] : add CHANGELOG

## 0.3.0 (2022-03-22)


- [REL]: version 0.2.0 → 0.3.0
- [IMP] docs: add gif example
- [REL]: version 0.1.3 → 0.2.0
- [IMP] : add tasks and issues option
- [MOV] : move module to own folder
- [REL]: version 0.1.2 → 0.1.3
- [IMP] : implement commitizen methods
- add methods `example`, `schema` and `info`
- [IMP] : add config files
- bump: version 0.1.1 → 0.1.2
- [FIX] : add whitespace in commits multiline description
- Also update docs with installation info and fix the repo URL
- [FIX] metadata: fix repository url
- feat: adapt commitizen to OCA guidelines for commits
